package com.nycschools.application.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nycschools.application.databinding.ItemSchoolListBinding
import com.nycschools.application.models.SchoolInfo
import java.text.DecimalFormat

/**
 * A recyclerview adapter class to show the list of schools.
 */
class SchoolListRVAdapter(
    private val onClickListener: OnItemClickListener,
) : RecyclerView.Adapter<SchoolListRVAdapter.ViewHolder>() {

    var schoolInfos = mutableListOf<SchoolInfo>()

    /**
     * Method to set school info.
     */
    fun setSchoolInfo(schoolInfos: List<SchoolInfo>) {
        this.schoolInfos = schoolInfos.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemSchoolListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val schoolInfo = schoolInfos[position]
        holder.getBinder().tvSchoolName.text = schoolInfo.school_name
        holder.getBinder().tvGraduationRateCount.text =
            if (schoolInfo.graduation_rate.isNullOrEmpty()) {
                "No data available"
            } else {
                DecimalFormat("#.##").format(
                    schoolInfo.graduation_rate.toBigDecimal(),
                )
            }
        holder.getBinder().tvSchoolCity.text = schoolInfo.city
        holder.getBinder().tvSchoolTotalStudents.text = schoolInfo.total_students

        holder.getBinder().root.setOnClickListener {
            onClickListener.onClick(position, schoolInfo)
        }
    }

    override fun getItemCount(): Int {
        return schoolInfos.size
    }

    /**
     * ViewHolder class to hold and bind school detail item.
     */
    class ViewHolder(private val binding: ItemSchoolListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun getBinder() = binding
    }

    interface OnItemClickListener {
        fun onClick(position: Int, schoolInfo: SchoolInfo)
    }
}
