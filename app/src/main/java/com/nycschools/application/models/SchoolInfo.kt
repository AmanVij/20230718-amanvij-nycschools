package com.nycschools.application.models

/**
 * Data class for school list api response.
 */
data class SchoolInfo(
    val advancedplacement_courses: String,
    val attendance_rate: String,
    val city: String,
    val dbn: String,
    val extracurricular_activities: String,
    val graduation_rate: String,
    val language_classes: String,
    val latitude: String,
    val location: String,
    val longitude: String,
    val overview_paragraph: String,
    val phone_number: String,
    val primary_address_line_1: String,
    val school_email: String,
    val school_name: String,
    val school_sports: String,
    val state_code: String,
    val total_students: String,
    val website: String,
    val zip: String,
)
