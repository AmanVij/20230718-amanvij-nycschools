package com.nycschools.application.views.fragments.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil.inflate
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.nycschools.application.R
import com.nycschools.application.databinding.BottomSheetDialogSatScoreBinding
import com.nycschools.application.databinding.FragmentSchoolDetailsBinding
import com.nycschools.application.models.SATScoreInfo
import com.nycschools.application.utils.AppActions
import com.nycschools.application.utils.AppMessage
import com.nycschools.application.viewmodels.SchoolViewModel
import org.koin.androidx.viewmodel.ext.android.activityViewModel

/**
 * Fragment to show school details.
 */
class SchoolDetailsFragment : Fragment(), AppMessage.AppMessageDialogListener {
    companion object {
        fun newInstance() = SchoolListFragment()
    }

    private val viewModel: SchoolViewModel by activityViewModel()
    private lateinit var binding: FragmentSchoolDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        // Inflate the layout for this fragment
        binding =
            inflate(inflater, R.layout.fragment_school_details, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        binding.parentView.setOnClickListener { }

        setupView()
        return binding.root
    }

    /**
     * Method to initialise and setup the screen components.
     */
    private fun setupView() {
        viewModel.selectedSchoolInfo.value?.let { schoolInfo ->
            binding.btnSatScore.setOnClickListener {
                viewModel.fetchSATScore()
            }
            binding.ivPhone.setOnClickListener {
                AppActions.makeCall(this.requireContext(), schoolInfo.phone_number)
            }
            binding.ivEmail.setOnClickListener {
                AppActions.sendEmail(this.requireContext(), schoolInfo.school_email)
            }
            binding.ivNavigate.setOnClickListener {
                AppActions.navigateTo(this.requireContext(), schoolInfo.latitude, schoolInfo.longitude)
            }
            viewModel.satScoreList.observe(viewLifecycleOwner) {
                if (!it.isNullOrEmpty()) {
                    showSatScoreBottomSheetDialog(it[0])
                }
            }
            viewModel.errorMessageSATScore.observe(viewLifecycleOwner) { errorMessage ->
                if (errorMessage != null) {
                    AppMessage.showErrorDialog(
                        context = requireContext(),
                        listener = this@SchoolDetailsFragment,
                        positiveBtnText = "Retry",
                        negativeBtnText = "Cancel",
                        message = errorMessage,
                    )
                }
            }
        }
    }

    /**
     * Method to launch bottom sheet with school's SAT score.
     *
     * @param satScoreInfo - SAT score information of school.
     */
    private fun showSatScoreBottomSheetDialog(satScoreInfo: SATScoreInfo) {
        val bottomSheetDialog = BottomSheetDialog(requireContext())
        val bindingSheet = inflate<BottomSheetDialogSatScoreBinding>(
            layoutInflater,
            R.layout.bottom_sheet_dialog_sat_score,
            null,
            false,
        )
        bottomSheetDialog.setContentView(bindingSheet.root)

        bindingSheet.tvTotalSatApplicantsCount.text = satScoreInfo.num_of_sat_test_takers
        bindingSheet.tvSatCriticalReadingAvgScoreCount.text =
            satScoreInfo.sat_critical_reading_avg_score
        bindingSheet.tvSatMathAvgScoreCount.text = satScoreInfo.sat_math_avg_score
        bindingSheet.tvSatWritingAvgScoreCount.text = satScoreInfo.sat_writing_avg_score

        bottomSheetDialog.show()

        bottomSheetDialog.setOnDismissListener { viewModel.resetSatScoreInfo() }
    }

    override fun onPositiveDialogButtonClicked() {
        viewModel.resetSatScoreInfo() // Reset SAT score information
        viewModel.fetchSATScore()
    }

    override fun onNegativeDialogButtonClicked() {
        viewModel.resetSatScoreInfo() // Reset SAT score information
    }
}
