package com.nycschools.application.views.fragments.home

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.nycschools.application.R
import com.nycschools.application.adapters.SchoolListRVAdapter
import com.nycschools.application.databinding.FragmentSchoolListBinding
import com.nycschools.application.models.SchoolInfo
import com.nycschools.application.utils.AppMessage
import com.nycschools.application.viewmodels.SchoolViewModel
import com.nycschools.application.views.activities.NYCSchoolsHomeActivity
import org.koin.androidx.viewmodel.ext.android.activityViewModel

/**
 * Fragment to show list of schools.
 */
class SchoolListFragment : Fragment(), AppMessage.AppMessageDialogListener {

    companion object {
        fun newInstance() = SchoolListFragment()
    }

    val viewModel: SchoolViewModel by activityViewModel()
    private lateinit var binding: FragmentSchoolListBinding
    private lateinit var schoolListRVAdapter: SchoolListRVAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding =
            DataBindingUtil.inflate(layoutInflater, R.layout.fragment_school_list, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        setupView()
        viewModel.fetchSchools()
        return binding.root
    }

    /**
     * Method to initialise and setup the screen components.
     */
    private fun setupView() {
        // initialise and load data in recycler view
        schoolListRVAdapter = SchoolListRVAdapter(
            object : SchoolListRVAdapter.OnItemClickListener {
                override fun onClick(position: Int, schoolInfo: SchoolInfo) {
                    viewModel.setSelectedSchoolInfo(schoolInfo)
                    (activity as NYCSchoolsHomeActivity).navigateToDetailsFragment()
                }
            },
        )
        binding.rvSchoolList.layoutManager = LinearLayoutManager(requireContext())
        binding.rvSchoolList.adapter = schoolListRVAdapter

        viewModel.isLoadingSchoolInfo.observe(viewLifecycleOwner) {
            binding.loader.visibility = if (it) View.VISIBLE else View.GONE
        }

        viewModel.errorMessageSchoolInfo.observe(viewLifecycleOwner) { errorMessage ->
            errorMessage?.let { message ->
                AppMessage.showErrorDialog(
                    context = requireContext(),
                    listener = this@SchoolListFragment,
                    positiveBtnText = "Retry",
                    message = message,
                )
            }
        }

        viewModel.schoolList.observe(viewLifecycleOwner) {
            if (it != null) {
                schoolListRVAdapter.setSchoolInfo(it)
            }
        }
    }

    /**
     * Method to show an error dialog
     *
     * @param context - Current context
     * @param mess - Message string to be shown on error dialog
     */
    private fun showErrorDialog(context: Context, mess: String) {
        // Create a new instance of the AlertDialog.Builder class
        val builder = AlertDialog.Builder(context)
        builder.setTitle(getString(R.string.error_dialog_title)) // Set the title
        builder.setMessage(mess) // Set the message of the dialog
        builder.setPositiveButton("Retry") { _, _ ->
            viewModel.fetchSchools() // Retrying fetching the list of school
        }
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()
    }

    override fun onPositiveDialogButtonClicked() {
        viewModel.fetchSchools() // Retrying fetching the list of school
    }

    override fun onNegativeDialogButtonClicked() {
    }
}
