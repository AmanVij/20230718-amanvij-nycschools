package com.nycschools.application.views.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.add
import androidx.fragment.app.commit
import com.nycschools.application.R
import com.nycschools.application.databinding.ActivityNycSchoolsHomeBinding
import com.nycschools.application.views.fragments.home.SchoolDetailsFragment
import com.nycschools.application.views.fragments.home.SchoolListFragment

/**
 * Home Activity, which holds the application view to show the list of school and it's details.
 */
class NYCSchoolsHomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityNycSchoolsHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_nyc_schools_home)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, SchoolListFragment.newInstance())
                .commitNow()
        }
        // Initialize and setup toolbar.
        binding.toolbar.setLogo(R.drawable.ic_nyc_logo)
        setSupportActionBar(binding.toolbar)
    }

    /**
     * Method to navigate from [SchoolListFragment] to [SchoolDetailsFragment].
     */
    fun navigateToDetailsFragment() {
        supportFragmentManager.commit {
            setReorderingAllowed(true)
            addToBackStack(null)
            add<SchoolDetailsFragment>(R.id.container)
        }
    }
}
