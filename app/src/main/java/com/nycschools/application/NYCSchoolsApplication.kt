package com.nycschools.application

import android.app.Application
import com.nycschools.application.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext.startKoin

/**
 * Application class
 */
class NYCSchoolsApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        // Koin initialisation
        startKoin {
            androidLogger()
            androidContext(this@NYCSchoolsApplication)
            modules(appModule)
        }
    }
}
