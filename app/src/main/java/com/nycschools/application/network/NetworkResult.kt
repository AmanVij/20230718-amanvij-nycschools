package com.nycschools.application.network

/**
 * A sealed class for network result, defines the possible results from a network call.
 */
sealed class NetworkResult<T>(
    val data: T? = null,
    val message: String? = null,
    val responseCode: Int? = null,
) {
    class Success<T>(data: T?) : NetworkResult<T>(data)
    class Failure<T>(message: String, responseCode: Int? = null, data: T? = null) :
        NetworkResult<T>(data, message, responseCode)
}
