package com.nycschools.application.network

import com.nycschools.application.models.SATScoreInfo
import com.nycschools.application.models.SchoolInfo
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * ApiService interface to communicate with remote server.
 */
interface ApiService {
    @GET("resource/s3k6-pzi2.json")
    suspend fun loadSchoolsInfo(): Response<List<SchoolInfo>>

    @GET("resource/f9bf-2cp4.json")
    suspend fun loadSATScore(@Query("dbn") dbn: String): Response<List<SATScoreInfo>>
}
