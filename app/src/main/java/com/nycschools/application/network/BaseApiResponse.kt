package com.nycschools.application.network

import retrofit2.Response

/**
 * An abstract class to check/filter the api response.
 */
abstract class BaseApiResponse {
    suspend fun <T> safeCall(call: suspend () -> Response<T>): NetworkResult<T> {
        try {
            val response = call()
            if (response.isSuccessful) {
                val body = response.body()
                body.let { return NetworkResult.Success(body) }
            }
            return NetworkResult.Failure(response.message(), response.code())
        } catch (e: Exception) {
            return NetworkResult.Failure(e.message ?: e.toString())
        }
    }
}
