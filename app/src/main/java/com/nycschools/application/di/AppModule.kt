package com.nycschools.application.di

import com.nycschools.application.network.ApiService
import com.nycschools.application.repositories.SchoolRepository
import com.nycschools.application.repositories.SchoolRepositoryImpl
import com.nycschools.application.utils.AppConstants
import com.nycschools.application.viewmodels.SchoolViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Koin module to provide Networking, Repositories and ViewModel objects.
 */
val appModule = module {
    // Provide an instance of Retrofit client with singleton scope
    single {
        Retrofit.Builder()
            .baseUrl(AppConstants.BaseURL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService::class.java)
    }
    // Provide an instance of Repository in singleton scope.
    single<SchoolRepository> {
        SchoolRepositoryImpl(get())
    }
    // Provide view model instance.
    viewModel {
        SchoolViewModel(get())
    }
}
