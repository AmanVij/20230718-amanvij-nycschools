package com.nycschools.application.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nycschools.application.models.SATScoreInfo
import com.nycschools.application.models.SchoolInfo
import com.nycschools.application.network.NetworkResult
import com.nycschools.application.repositories.SchoolRepository
import kotlinx.coroutines.launch

/**
 * A view model class to handle necessary operations
 * on the UI layer and load school information from the repository.
 */
class SchoolViewModel(private val repository: SchoolRepository) : ViewModel() {
    private val _isLoadingSchoolInfo: MutableLiveData<Boolean> = MutableLiveData(false)
    val isLoadingSchoolInfo: LiveData<Boolean> = _isLoadingSchoolInfo

    private val _schoolList: MutableLiveData<List<SchoolInfo>?> = MutableLiveData(listOf())
    val schoolList: LiveData<List<SchoolInfo>?> = _schoolList

    private val _errorMessageSchoolInfo: MutableLiveData<String?> = MutableLiveData(null)
    val errorMessageSchoolInfo: LiveData<String?> = _errorMessageSchoolInfo

    private val _selectedSchoolInfo: MutableLiveData<SchoolInfo?> = MutableLiveData(null)
    val selectedSchoolInfo: LiveData<SchoolInfo?> = _selectedSchoolInfo

    private val _isLoadingSATScore: MutableLiveData<Boolean> = MutableLiveData(false)
    val isLoadingSATScore: LiveData<Boolean> = _isLoadingSATScore

    private val _satScoreList: MutableLiveData<List<SATScoreInfo>?> = MutableLiveData(listOf())
    val satScoreList: LiveData<List<SATScoreInfo>?> = _satScoreList

    private val _errorMessageSATScore: MutableLiveData<String?> = MutableLiveData(null)
    val errorMessageSATScore: LiveData<String?> = _errorMessageSATScore

    /**
     * Method to fetch the list of schools in NYC.
     */
    fun fetchSchools() {
        viewModelScope.launch {
            _isLoadingSchoolInfo.value = true
            repository.loadSchoolsInfo().collect {
                _isLoadingSchoolInfo.value = false
                when (it) {
                    is NetworkResult.Success -> {
                        if (it.data.isNullOrEmpty()) {
                            _errorMessageSchoolInfo.value = "No data received from server."
                        }
                        _schoolList.value = it.data
                    }

                    is NetworkResult.Failure -> {
                        _errorMessageSchoolInfo.value = it.message
                    }
                }
            }
        }
    }

    /**
     * Method to set/hold the selected school information.
     */
    fun setSelectedSchoolInfo(schoolInfo: SchoolInfo) {
        _selectedSchoolInfo.value = schoolInfo
    }

    /**
     * Method to fetch SAT Score for the given "dpn"
     */
    fun fetchSATScore() {
        viewModelScope.launch {
            _isLoadingSATScore.value = true
            selectedSchoolInfo.value?.let {
                repository.loadSATScore(it.dbn).collect {
                    _isLoadingSATScore.value = false
                    when (it) {
                        is NetworkResult.Success -> {
                            if (it.data.isNullOrEmpty()) {
                                _errorMessageSATScore.value = "No data received from server."
                            }
                            _satScoreList.value = it.data
                        }

                        is NetworkResult.Failure -> {
                            Log.e("API error:", "${it.responseCode}: ${it.message}")
                            _errorMessageSATScore.value = it.message
                        }
                    }
                }
            }
        }
    }

    /**
     * Method to return address in desired format
     */
    fun getAddress(schoolInfo: SchoolInfo) =
        "${schoolInfo.primary_address_line_1}, ${schoolInfo.city}, ${schoolInfo.state_code} ${schoolInfo.zip}"

    /**
     * Method to validate the field
     */
    fun getDefaultValueIfEmpty(field: String?) = if (field.isNullOrEmpty()) "NA" else field

    /**
     * Method to reset the SAT Score values.
     */
    fun resetSatScoreInfo() {
        _satScoreList.value = emptyList()
        _errorMessageSATScore.value = null
    }
}
