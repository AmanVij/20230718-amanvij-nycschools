package com.nycschools.application.utils

import android.content.Context
import android.content.Intent
import android.net.Uri

object AppActions {
    /**
     * Method to launch dial intent
     * @param context - Context used for launching the intent
     * @param phoneNumber - Phone number to dial
     */
    fun makeCall(context: Context, phoneNumber: String) {
        context.startActivity(
            Intent(
                Intent.ACTION_DIAL,
                Uri.fromParts("tel", phoneNumber, null),
            ),
        )
    }

    /**
     * Method to launch email intent
     * @param context - Context used for launching the intent
     * @param email - Receiver email id
     */
    fun sendEmail(context: Context, email: String) {
        val emailIntent =
            Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:$email"))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "subject")
        emailIntent.putExtra(Intent.EXTRA_TEXT, "body")
        context.startActivity(Intent.createChooser(emailIntent, "Send email..."))
    }

    /**
     * Method to launch Google navigate from current location to the provided lat/long.
     * @param context - Context used for launching the intent
     * @param latitude - Destination's latitude
     * @param longitude - Destination's longitude
     */
    fun navigateTo(context: Context, latitude: String, longitude: String) {
        val gmmIntentUri =
            Uri.parse("google.navigation:q=$latitude,$longitude")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        context.startActivity(mapIntent)
    }
}
