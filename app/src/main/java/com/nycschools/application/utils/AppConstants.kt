package com.nycschools.application.utils

/**
 * App constants.
 */
object AppConstants {
    const val BaseURL = "https://data.cityofnewyork.us/"
}
