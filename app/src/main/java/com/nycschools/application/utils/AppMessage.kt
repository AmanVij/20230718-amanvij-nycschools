package com.nycschools.application.utils

import android.content.Context
import androidx.appcompat.app.AlertDialog
import com.nycschools.application.R

object AppMessage {

    /**
     * Method to show an error dialog
     *
     * @param context - Current context
     * @param listener - Listener to listen dialog button click
     * @param positiveBtnText - Text for positive dialog button
     * @param negativeBtnText - Text for negative dialog button
     * @param message - Message string to be shown on error dialog
     */
    fun showErrorDialog(
        context: Context,
        listener: AppMessageDialogListener,
        positiveBtnText: String,
        negativeBtnText: String? = null,
        message: String,
    ) {
        // Create a new instance of the AlertDialog.Builder class
        val builder = AlertDialog.Builder(context)
        builder.setTitle(context.getString(R.string.error_dialog_title)) // Set the title
        builder.setMessage(message) // Set the message of the dialog
        builder.setPositiveButton(positiveBtnText) { _, _ ->
            listener.onPositiveDialogButtonClicked() // Retrying fetching the list of school
        }
        if (!negativeBtnText.isNullOrEmpty()) {
            builder.setNegativeButton("Cancel") { dialog, _ ->
                listener.onNegativeDialogButtonClicked()
                dialog.dismiss() // Close or dismiss the alert dialog
            }
        }
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()
    }

    interface AppMessageDialogListener {
        fun onPositiveDialogButtonClicked()
        fun onNegativeDialogButtonClicked()
    }
}
