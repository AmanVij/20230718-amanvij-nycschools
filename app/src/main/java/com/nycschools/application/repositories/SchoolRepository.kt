package com.nycschools.application.repositories

import com.nycschools.application.models.SATScoreInfo
import com.nycschools.application.models.SchoolInfo
import com.nycschools.application.network.NetworkResult
import kotlinx.coroutines.flow.Flow

/**
 * A repository interface that handles remote data operations for retrieving school information.
 */
interface SchoolRepository {
    /**
     * Loads the list of all the schools available in NYC.
     *
     * @return Api response with the list of school.
     */
    suspend fun loadSchoolsInfo(): Flow<NetworkResult<List<SchoolInfo>>>

    /**
     * Loads the list of SAT Score for the provided schools id (dbn).
     * @param dbn - A unique school id
     *
     * @return Api response with the list of SAT score.
     */
    suspend fun loadSATScore(dbn: String): Flow<NetworkResult<List<SATScoreInfo>>>
}
