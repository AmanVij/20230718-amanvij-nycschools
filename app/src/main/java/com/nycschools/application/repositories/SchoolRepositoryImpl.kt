package com.nycschools.application.repositories

import com.nycschools.application.models.SATScoreInfo
import com.nycschools.application.models.SchoolInfo
import com.nycschools.application.network.ApiService
import com.nycschools.application.network.BaseApiResponse
import com.nycschools.application.network.NetworkResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

/**
 * An implementation of [SchoolRepository], to handle
 * remote data operations for retrieving SAT Score.
 */
class SchoolRepositoryImpl(val apiService: ApiService) : SchoolRepository, BaseApiResponse() {
    override suspend fun loadSchoolsInfo(): Flow<NetworkResult<List<SchoolInfo>>> {
        return flow {
            emit(safeCall({ apiService.loadSchoolsInfo() }))
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun loadSATScore(dbn: String): Flow<NetworkResult<List<SATScoreInfo>>> {
        return flow {
            emit(safeCall({ apiService.loadSATScore(dbn) }))
        }.flowOn(Dispatchers.IO)
    }
}
