package com.nycschools.application.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nycschools.application.models.SATScoreInfo
import com.nycschools.application.models.SchoolInfo
import com.nycschools.application.network.NetworkResult
import com.nycschools.application.repositories.SchoolRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`

@ExperimentalCoroutinesApi
class SchoolViewModelTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()
    private val testDispatcher = UnconfinedTestDispatcher()
    private val repository = mock(SchoolRepository::class.java)
    private lateinit var viewModel: SchoolViewModel

    @Before
    fun setup() {
        viewModel = SchoolViewModel(repository)
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `should fetch the list of schools with data`() = runBlocking {
        val mockSchoolList = listOf(
            SchoolInfo(
                advancedplacement_courses = "Temp course",
                attendance_rate = "0.99",
                city = "Manhattan",
                dbn = "123",
                extracurricular_activities = "Temp activity",
                graduation_rate = "0.77",
                language_classes = "English",
                latitude = "0.123435",
                location = "Manhattan",
                longitude = "0.1234566",
                overview_paragraph = "Desc",
                phone_number = "9999999999",
                primary_address_line_1 = "Address",
                school_email = "abc@xyz.com",
                school_name = "ABC School",
                school_sports = "Football",
                state_code = "123456",
                total_students = "250",
                website = "www.abc.com",
                zip = "123456",
            ),
        )
        `when`(repository.loadSchoolsInfo()).thenReturn(flowOf(NetworkResult.Success(mockSchoolList)))
        viewModel.fetchSchools()
        assertEquals(mockSchoolList, viewModel.schoolList.value)
    }

    @Test
    fun `should fail to fetch the list of schools`() = runBlocking {
        `when`(repository.loadSchoolsInfo()).thenReturn(
            flowOf(
                NetworkResult.Failure(
                    "Test error",
                ),
            ),
        )
        viewModel.fetchSchools()
        assertEquals("Test error", viewModel.errorMessageSchoolInfo.value)
    }

    @Test
    fun `should fetch the SAT Score with dpn id of the schools`() = runBlocking {
        val mockSATScoreInfo = listOf(
            SATScoreInfo(
                dbn = "123",
                school_name = "Test School",
                num_of_sat_test_takers = "20",
                sat_math_avg_score = "190",
                sat_critical_reading_avg_score = "230",
                sat_writing_avg_score = "190",
            ),
        )
        `when`(repository.loadSATScore("123")).thenReturn(
            flowOf(
                NetworkResult.Success(
                    mockSATScoreInfo,
                ),
            ),
        )
        viewModel.setSelectedSchoolInfo(
            SchoolInfo(
                advancedplacement_courses = "Temp course",
                attendance_rate = "0.99",
                city = "Manhattan",
                dbn = "123",
                extracurricular_activities = "Temp activity",
                graduation_rate = "0.77",
                language_classes = "English",
                latitude = "0.123435",
                location = "Manhattan",
                longitude = "0.1234566",
                overview_paragraph = "Desc",
                phone_number = "9999999999",
                primary_address_line_1 = "Address",
                school_email = "abc@xyz.com",
                school_name = "ABC School",
                school_sports = "Football",
                state_code = "123456",
                total_students = "250",
                website = "www.abc.com",
                zip = "123456",
            ),
        )

        viewModel.fetchSATScore()
        assertEquals(mockSATScoreInfo, viewModel.satScoreList.value)
    }
}
