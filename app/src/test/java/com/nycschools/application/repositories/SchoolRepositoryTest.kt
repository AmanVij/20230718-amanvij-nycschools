package com.nycschools.application.repositories

import com.nycschools.application.models.SATScoreInfo
import com.nycschools.application.models.SchoolInfo
import com.nycschools.application.network.ApiService
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.Mockito.anyString
import org.mockito.Mockito.`when`
import org.mockito.kotlin.mock
import retrofit2.Response

@ExperimentalCoroutinesApi
class SchoolRepositoryTest {

    private val apiService = mock<ApiService>()
    private val schoolRepository = SchoolRepositoryImpl(apiService)

    @Test
    fun `should return a list of SchoolInfo when data is available`() = runBlocking {
        val mockResponse = Response.success(
            listOf<SchoolInfo>(
                SchoolInfo(
                    advancedplacement_courses = "Temp course",
                    attendance_rate = "0.99",
                    city = "Manhattan",
                    dbn = "123",
                    extracurricular_activities = "Temp activity",
                    graduation_rate = "0.77",
                    language_classes = "English",
                    latitude = "0.123435",
                    location = "Manhattan",
                    longitude = "0.1234566",
                    overview_paragraph = "Desc",
                    phone_number = "9999999999",
                    primary_address_line_1 = "Address",
                    school_email = "abc@xyz.com",
                    school_name = "ABC School",
                    school_sports = "Football",
                    state_code = "123456",
                    total_students = "250",
                    website = "www.abc.com",
                    zip = "123456",
                ),
            ),
        )
        `when`(apiService.loadSchoolsInfo()).thenReturn(mockResponse)
        val result = schoolRepository.loadSchoolsInfo()
        assertEquals(mockResponse.body(), result.single().data)
    }

    @Test
    fun `should return SAT Score for the provided school with dpn `() = runBlocking {
        val satScoreList = listOf(
            SATScoreInfo(
                dbn = "123",
                school_name = "Test School",
                num_of_sat_test_takers = "20",
                sat_math_avg_score = "190",
                sat_critical_reading_avg_score = "230",
                sat_writing_avg_score = "190",
            ),
        )
        val mockResponse: Response<List<SATScoreInfo>> = Response.success(satScoreList)
        `when`(apiService.loadSATScore(anyString())).thenReturn(mockResponse)
        val result = schoolRepository.loadSATScore("123")
        assertEquals(mockResponse.body(), result.single().data)
    }
}
