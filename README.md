# 20230718-AmanVij-NYCSchools

## NYC School App
---

NYC School application which shows list of schools in NYC area. The app is build in Native Android using MVVM architecture.
</br>

## Tech stack & Open-source libraries 
---

- Minimum SDK level 24
- [Kotlin](https://kotlinlang.org/)
  based
- [Coroutines](https://github.com/Kotlin/kotlinx.coroutines)
- [Flow](https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines.flow/)
  for asynchronous.
- [Koin](https://insert-koin.io/) for dependency injection.
- JetPack
  - ViewModel - UI related data holder, lifecycle aware.
  - LiveData - Observe state changes
- Architecture
  - MVVM Architecture (Model - View - ViewModel)</br> 
  ![MVVM Architecture](/snapshots/MVVM_Architecture.png)
- [Retrofit2](https://github.com/square/retrofit) - construct the REST APIs.
- [Mockito](https://site.mockito.org/) - mocking framework for unit testing.

## Screenshots
---

<table>
  <td>
    <p align="center">
<h3>School List Screen</h1>
      [<img src="/snapshots/school_list_screen.png" alt="School List Screen" width="400" height="800"/>]
    </p>
  </td>
  <td>
    <p align="center">
<h3>School Detail Screen</h1>
      <img src="/snapshots/school_detail_screen.png" alt="School Detail Screen" width="400" height="800"/>
    </p>
  </td>
  <td>
    <p align="center">
<h3>School SAT Score Screen</h1>
      <img src="/snapshots/school_sat_score_screen.png" alt="School SAT Score Screen" width="400" height="800"/>
    </p>
  </td>
</table>
